using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using NUnit.Framework;

namespace test_sqlite3_windows
{
  public class TestForms
  {
    [Test]
    public void TestForm1() {
      Application.SetHighDpiMode(HighDpiMode.SystemAware);
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      // Start thread to close form after time.
      {
        Form1 form1 = new Form1();
        Task.Run(async () => {
          await Task.Delay(3000);
          form1.Close();
        });
        Task.Run(async () => {
          await Task.Delay(5000);
          if (form1.Visible) {
            Assert.Fail("Form1 has not been closed.");
          }
        });
        Application.Run(form1);
      }
    }
  }
}
